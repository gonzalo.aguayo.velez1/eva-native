export interface Evaluation {
  evaluationId: string;
  userId: string;
  schemaId: string;
  answerId: string;
  userName: string;
  userEmail: string;
  evaluationName: string;
  startDate: string;
  endDate: string;
  answerDate: string;
  score: number;
}

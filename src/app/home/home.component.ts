import { Component, OnInit } from '@angular/core';
import { isAndroid } from 'tns-core-modules/platform';
import * as moment from 'moment';
import * as camera from 'nativescript-camera';

import { Evaluation } from '../models/evaluation';
import { EvaluationsService } from '../shared/evaluations.service';
import { Page } from 'tns-core-modules/ui/page/page';

@Component({
  selector: 'Home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  items: Evaluation[];
  isLoading = false;
  profileImage: string = 'https://i.imgur.com/q3HNpr5.png';

  constructor(
    private page: Page,
    private evaluationService: EvaluationsService,
  ) {
    this.page.actionBarHidden = true;
  }

  ngOnInit(): void {
    this.isLoading = true;

    this.evaluationService.getAllEvaluations().subscribe((evaluations: Evaluation[]) => {
      this.items = [...evaluations, ...evaluations, ...evaluations];
      this.isLoading = false;
    });
  }

  getEndDate(endDate: string): string {
    return moment(endDate).fromNow();
  }

  takePicture(): void {
    camera.takePicture()
      .then((imageAsset) => {
        this.profileImage = isAndroid ? imageAsset.android : imageAsset.ios;
        console.log(this.profileImage);
      });
  }
}

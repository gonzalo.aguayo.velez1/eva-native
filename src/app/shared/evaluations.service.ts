import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Evaluation } from '../models/evaluation';

@Injectable({
  providedIn: 'root',
})
export class EvaluationsService {
  private BASE_URL = 'https://webapi.evapro.online/api/v1/tests';
  private TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJZCI6IjQxIiwiTmFtZSI6IkdvbnphbG8gQWd1YXlvIiwiTGFzdE5hbWUiOiJWZWxleiIsIkVtYWlsIjoiZ29uemFsby5hZ3VheW8uamFsYUBnbWFpbC5jb20iLCJSb2xlIjoiQURNSU4iLCJVc2VybmFtZSI6ImdvbnphbG9iOGY0IiwiQXZhdGFyIjoiaHR0cHM6Ly9saDQuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy0wc19ETVZWeERiYy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFDVS9aVEFqVVBzMDh4TS9zNTAvcGhvdG8uanBnIiwiQ29uZmlybWVkQWNjb3VudCI6IlRydWUiLCJpc3MiOiJKYWxhc29mdCIsImF1ZCI6WyJKYWxhc29mdCIsIkphbGFzb2Z0Il0sImV4cCI6MTYwNDI0NDIzNiwiUGVybWlzc2lvbiI6WyJmaWxlOjE1IiwicHJlc2VudGF0aW9uOjE1IiwiZXZhbHVhdGlvbjoxNSIsInRlbXBsYXRlOjE1Iiwic2NoZW1hOjE1IiwidXNlcjoxNSIsInJvbGU6MTUiLCJpbnZpdGU6MTUiLCJteXRlc3Q6MTUiLCJzY29yZToxNSIsImFuc3dlcjoxNSJdfQ.Besytm3QuA19uxT50LBglnbftOXu07l2rXDlAgols-U';

  constructor(private http: HttpClient) { }

  getAllEvaluations(pageNumber = 1, pageSize = 9): Observable<Evaluation[]> {
    const params = {
      'page[number]': pageNumber.toString(),
      'page[size]': pageSize.toString(),
      'filter[name]': '',
    };
    const headers = {
      Authorization: `Bearer ${this.TOKEN}`,
    };

    return this.http.get<Evaluation[]>(this.BASE_URL, { params, headers });
  }
}
